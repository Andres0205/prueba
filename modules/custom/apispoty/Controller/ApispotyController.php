namespace Drupal\apispoty\Controller;
 
use Drupal;
use Drupal\node\Entity\Node;
use Drupal\Core\Controller\ControllerBase;
 
class ApispotyController extends ControllerBase {
     
// Página 1
  public function content() {
    return array(
        '#type' => 'markup',
        '#markup' => $this->t('Hola mundo !!'),
    );
  }
   
// Página 2
  public function getListados() {
       
      // Sacar datos de la base de datos
        $query = Drupal::entityQuery('node')
                ->condition('type', 'lanzamientos')
                ->execute();
 
      // Recorrer los resultados y guardar los nodos en un array
        if (!empty($query)) {
            foreach ($query as $listadoId) {
                $listado = Node::load($listadoId);
                $listados[] = $listado;
            }
        }
         
        // Cargamos datos en una vísta, usando hook_theme
        return array(
            '#theme' => 'show_listado',
            '#titulo' => $this->t('Listado de lanzamientos'),
            '#descripcion' => $this->t('lanzamientos disponibles'),
            '#listados' => $listados          
        );
    }
   
}
 
?>
